package instrumentor;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.commons.AdviceAdapter;

public class ModifiedAdviceAdapter extends AdviceAdapter {

	String name;
	String desc;
	public static String className;

	public ModifiedAdviceAdapter(int api, int access, String name, String desc, MethodVisitor mv) {

		super(Opcodes.ASM4, mv, access, name, desc);
		this.name = name;
		this.desc = desc;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.objectweb.asm.commons.AdviceAdapter#onMethodExit(int)
	 */
	@Override
	protected void onMethodExit(int opcode) {
		mv.visitLdcInsn("EventHandler");

		/** alternative instrumentation with tid ***/
		mv.visitTypeInsn(NEW, "java/lang/StringBuilder");
		mv.visitInsn(DUP);
		mv.visitMethodInsn(INVOKESPECIAL, "java/lang/StringBuilder", "<init>", "()V");
		mv.visitLdcInsn("Finished_" + className + "_" + name + desc + "=");
		mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append",
				"(Ljava/lang/String;)Ljava/lang/StringBuilder;");
		mv.visitMethodInsn(INVOKESTATIC, "android/os/Process", "myTid", "()I");
		mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "append", "(I)Ljava/lang/StringBuilder;");
		mv.visitMethodInsn(INVOKEVIRTUAL, "java/lang/StringBuilder", "toString", "()Ljava/lang/String;");

		/** old instrumentation without tid ***/

		// mv.visitLdcInsn("Finished_" + name + desc);
		mv.visitMethodInsn(INVOKESTATIC, "android/util/Log", "i", "(Ljava/lang/String;Ljava/lang/String;)I");
		mv.visitInsn(POP);

	}
}
