package instrumentor;

import java.io.File;
import java.io.IOException;
import java.util.List;

import javax.swing.SwingWorker;

import org.apache.commons.io.FileUtils;

public class MyWorker extends SwingWorker<Integer, String> {

	@Override
	protected Integer doInBackground() throws Exception {
		InstrumentPanel.jTextArea1.append("Instrumenting system calls\n");

		Global.efgExtractMode = 0;
		ApkInstrumentor.instrument(Global.toInstrument);

		final String eventInstr = Global.pathToOutputFolder + File.separator + "signed-instrumented.apk";

		File fp = new File(eventInstr);

		final int idx = Global.toInstrument.indexOf(Global.selectedFolder);
		if (idx != -1) {
			Global.appName = Global.toInstrument.substring(idx + Global.selectedFolder.length());
		} else {
			Global.appName = "Instrumented.apk";
		}

		final File source = new File(eventInstr);
		Global.toInstrument = Global.selectedFolder + File.separator + "signed-instrumented.apk";
		final File dest = new File(Global.toInstrument);
		try {
			FileUtils.copyFile(source, dest);
		} catch (final IOException e) {
			e.printStackTrace();
		}

		System.gc();
		InstrumentPanel.jTextArea1.append("Instrumenting event handlers\n");
		Global.efgExtractMode = 1;
		ApkInstrumentor.instrument(Global.toInstrument);
		// delete the event instrumented file

		final String instrname = Global.selectedFolder + File.separator + Global.appName.replace(".apk", "")
				+ "_instr.apk";
		final File fp2 = new File(instrname);
		FileUtils.copyFile(fp, fp2);

		System.gc();
		fp = new File(Global.toInstrument);
		FileUtils.deleteQuietly(fp);

		fp = new File(Global.pathToOutputFolder);
		FileUtils.deleteDirectory(fp);

		InstrumentPanel.jTextArea1.append("\nInstrumented file is " + instrname + "\n");

		return 1;
	}

	@Override
	protected void process(List<String> chunks) {
		for (final String c : chunks) {
			InstrumentPanel.jTextArea1.append(c + "\n");
		}
	}

}
